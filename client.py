#!/usr/bin/python3
"""Programa cliente UDP que abre un socket a un servidor."""

import sys
import socket

# Constantes. Dirección IP del servidor y contenido a enviar
# Control de argumentos

try:
    ip = sys.argv[1]
    puerto = int(sys.argv[2])
    register = sys.argv[3]
    usuario = sys.argv[4]
    expira = int(sys.argv[5])
except IndexError:
    sys.exit("Usage: client.py ip puerto register sip_address expires_value")

# Generamos la peticion

if ip == 'localhost' or ip == '127.0.0.1':
    pass
else:
    sys.exit("Debe introducir localhost o 127.0.0.1")
if register == "REGISTER":
    mensaje = register + " sip:" + usuario + " SIP/2.0\r\n"
    mensaje2 = "Expires: " + str(expira) + "\r\n\r\n"
    linea = mensaje + mensaje2
else:
    sys.exit("Usage: client.py ip puerto register address expires")
"""
Constantes. Dirección IP del servidor y contenido a enviar
SERVER = 'localhost'
PORT = 6001
LINE = '¡Hola mundo!'

PORT += 1
Cuando lo ponemos en mayusculas es porque no queremos que se modifique
"""

# Creamos el socket, lo configuramos y lo atamos a un servidor/puerto
with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as my_socket:
    my_socket.connect((ip, puerto))
    print("Enviando:", linea)
    my_socket.send(bytes(linea, 'utf-8') + b'\r\n')
    data = my_socket.recv(1024)
    print(data.decode('utf-8'))
print("Socket terminado.")

"""
string       bytes
'a'   ----> 0101111 (ASCII 'virtual')
'a'   ----> 01010101010101 (otra codificacion)
"""
