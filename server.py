#!/usr/bin/python3
"""Clase (y programa principal) para un servidor de eco en UDP simple."""
import socketserver
import sys
import json
from datetime import datetime, date, time, timedelta
import time

fecha = "%Y-%m-%d %H:%M:%S"


class SIPRegisterHandler(socketserver.DatagramRequestHandler):
    """Echo server class."""

    mensaje = {}
    list = []

    def json2registered(self):
        """Comprobar que el archivo no este creado."""
        try:
            with open("registered.json", "r") as jsonfile:
                self.mensaje = json.load(jsonfile)
        except IndexError:
            """En caso de que no este creado continuamos para crearlo."""
            pass

    def register2json(self):
        """Abrir el fichero y escribir en el."""
        with open("registered.json", "w") as jsonfile:
            json.dump(self.mensaje, jsonfile, indent=3)

    def handle(self):
        """handle method of the server class."""
        self.json2registered()
        """Forma que saldrá en el fichero json"""
        nombre_usuario = {"address": "", "expires": ""}
        ipcliente = self.client_address[0]
        print("IP CLIENTE: " + ipcliente + "\n\r")
        puertocliente = self.client_address[1]
        print("PUERTO CLIENTE: " + str(puertocliente) + "\r\n")
        """Añadimos una lista para almacenar el mensaje."""
        lista = []
        for line in self.rfile:
            print("El cliente nos manda: ", line.decode('utf-8'))
            lista.append(line.decode("utf-8"))
        # Actualizamos la lista existente con el mensaje mandado
        # En mensaje almacenamos el primer valor de la lista
        # separado por un espacio
        cadena = lista[0].split(" ")
        # print("El valor del mensaje es:" + mensaje)
        # Comprobamos que la peticion es REGISTER
        if cadena[0] == "REGISTER":
            usuario = cadena[1].split(":")[1]
            # Cortamos apartir de los :
            # solo nos quedamos con el nombre de usuario
            cadena = lista[1].split()
            expires = cadena[1].split("\r\n")[0]
            dato = datetime.now() + timedelta(seconds=int(expires))
            # En dato almacenamos la fecha actual + timedelta
            # timedelta: duracion que expresa la diferencia entre dos date
            nombre_usuario["address"] = ipcliente
            nombre_usuario["expires"] = dato.strftime(fecha)
            # Añadimos en el diccionario en los valores address y expires
            # strftime: cadena que representa la fecha en formato explicito
            # Si el expires es 0, se borra al cliente del diccionario
            self.wfile.write(b"SIP/2.0 200 OK\r\n\r\n")
            if int(expires) == 0:
                try:
                    del self.mensaje[usuario]
                    # Comprueba en el diccionario almacenado en json y lo borra
                    # Solo se mostrara si realmente borra
                    self.wfile.write(b"SIP/2.0 200 OK\r\n\r\n")
                except KeyError:
                    self.wfile.write(b"SIP/2.0 200 OK\r\n\r\n")
            # Si el expires es distinto de 0, se guarda al usuario en el dicc
            else:
                self.mensaje[usuario] = nombre_usuario
                #  print(self.mensaje)
            # Comprobamos si algun cliente tiene la sesion caducada.
            # Si es asi se borrará del diccionario.
            # hora = time.strftime(fecha).time.gmtime()
            hora_actual = datetime.now().strftime(fecha)
            # hora_actual nos devuelve la fecha,
            # la hora actual y establecemos el
            # formato que nos debe devolver
            #  print(hora_actual)
            usuario_borrado = []
            # Creamos una nueva lista para
            # almacenar los usuarios que borraremos
            for usuario in self.mensaje:
                if hora_actual >= self.mensaje[usuario]['expires']:
                    # Comprobamos si la hora actual es mayor al valor
                    # almacenado en el diccionario de expires
                    usuario_borrado.append(usuario)
                    # actualizamos la lista
                if hora_actual == self.mensaje[usuario]["expires"]:
                    del self.mensaje[usuario]
                    print("Este usuario ya ha caducado")
            for usuario in usuario_borrado:
                del self.mensaje[usuario]
                # Borramos el usuario
            self.register2json()
        self.wfile.write(b"Hemos recibido el mensaje")


if __name__ == "__main__":
    # Listens at localhost ('') port 6001
    # and calls the EchoHandler class to manage the request
    puerto_escucha = int(sys.argv[1])
    serv = socketserver.UDPServer(('', puerto_escucha), SIPRegisterHandler)

    print("Lanzando servidor UDP de eco...")
    try:
        serv.serve_forever()
    except KeyboardInterrupt:
        print("Finalizado servidor")
